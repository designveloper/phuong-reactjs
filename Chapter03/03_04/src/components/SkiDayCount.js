import "../stylesheets/ui.scss"

const createClass = require("create-react-class");

export const SkiDayCount = createClass({
    percentToDecimal(decimal) {
        return decimal * 100 + "%";
    },

    calcGoalProgress(total, goal) {
        return this.percentToDecimal(total/goal);
    },

    render() {
        return (
            <div className="ski-day-count">
                <div className="total-days">
                    <span>{this.props.total}</span>
                    <span>&nbsp;days</span>
                </div>
                <div className="powder-days">
                <span>{this.props.powder}</span>
                <span>&nbsp;days</span>
                </div>
                <div className="backcountry-days">
                <span>{this.props.backcountry}</span>
                <span>&nbsp;days</span>
                </div>
                <div>
                    <span>{this.calcGoalProgress(this.props.total, this.props.goal)}</span>
                </div>
            </div>
        );
    }
});
