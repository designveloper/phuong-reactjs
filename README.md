# [React.js Essential Training](https://www.lynda.com/React-js-tutorials/React-js-Essential-Training/496905-2.html)

## Introduction
* React is a javascript library that's used to build UI

## Chapter 01 - What is React.JS ?

### What is React ?
* React is a javascript library that's used to build UI
* Was create at Facebook and Instagram

### Setting up Chrome tools for React
* Add `node-detector` and `React Developer tools` extensions to Chrome

### Efficient rendering with React
* React made updating the DOM faster by using **DOM diffing**
    * Compare rendered contents with the new UI changes
    * Make only minimal changes necessory
    * Compare Javascript objects
    * Be faster than writing to or reading from the actual DOM

## Chapter 02 - Intro to JSX and Babel

### Pure React
* Import library in HTML file

```html
<script src="https://fb.me/react-15.1.0.js"></script>
<script src="https://fb.me/react-dom-15.1.0.js"></script>
```

* In `index.js`

```javascript
const title = React.createElement(
    "h1",
    {id: "title", className: "header"},
    "Hello World"
);

ReactDOM.render(
    title,
    document.getElementById("react-container")
);
```

### Refactoring elements using JSX
* JSX is javascript as XML

### Building with webpack
* Webpack:
    * Is a module bundler
    * Create static files
    * Automates processes
* `webpack.config.js`: describes configurations for webpack command

```javascript
module.exports = {...};
```

* When using `webpack-dev-server`, If we do any changes in files, it'll automatically rebuild the server and reload without refreshing. And it doesn't generate the bundle file in the storage, the bundle file is stored in memory

### Loading JSON with webpack
* We can load JSON by using `import alias from "<JSONfile>"` and adding some configurations to `webpack.config.js`

### Adding CSS to webpack build
* We also can import a css file. And we need the `css-loader` to process CSS files, `style-loader` to apply the styles in the CSS file
* We still have to add some configurations to `webpack.config.js`

## Chapter 03 - React Components

### Creating components with createClass()
* Example

```javascript
//component definition
export const Person = React.createClass({
    render() {
        return (
            <div className="person">
                <div className="name">
                    <span>John</span>
                </div>
                <div className="age">
                    <span>20</span>
                </div>
            </div>
        );
    }
});

//use component
render(
	<Person />,
	document.getElementById('info')
)
```

### Adding component properties
* Example

```javascript
//component definition
export const Person = React.createClass({
    render() {
        return (
            <div className="person">
                <div className="name">
                    <span>{this.props.name}</span>
                </div>
                <div className="age">
                    <span>{this.props.age}</span>
                </div>
            </div>
        );
    }
});

//use component
render(
	<Person name = "John"
            age = {20}/>,
	document.getElementById('info')
)
```

### Adding component methods
* Example

```javascript
React.createClass({
    method1() {...},
    method2() {...},
    render() {
        return (...);
    }
});
```

### Creating components with ES6 class syntax
* We just define a normal ES6 class, but it have to inherit from `React.Component`, and then our class will become a  React Component

### Creating stateless functional components
* Stateless functional components are functions that take in property information and return JSX elements

```javascript
const MyComponent = (props) => (
    <div>{props.title}</div>
)
```

### Adding react-icons
* Install package: `npm install --save react-icons`
* To use an icon, we'll import the path of that icon. Reference: [React Icons](https://github.com/gorangajic/react-icons)

## Chapter 04 - Props and State

### Default props
* If Use `createClass`, add one more function named `getDefaultProps()`

```javascript
export const SkiDayCount = createClass({
    getDefaultProps() {
        return {
            total: 50,
            powder: 50,
            backcountry: 15,
            goal: 100
        }
    },
    ...
})
```

* If Use ES6 class or stateless function:

```javascript

SkiDayCount.defaultProps = {
    total: 50,
    powder: 10,
    backcountry: 15,
    goal: 75
}
```

* But if use stateless function, we also use the default parameter for the function

### Validating with React.PropTypes
* If use createClass:

```javascript
export const SkiDayCount = createClass({
    propTypes: {
        total: PropTypes.number.isRequired,
        powder: PropTypes.number,
        backcountry: PropTypes.number,
        goal: PropTypes.number
    },
    ...
})
```

* If use ES6 and stateless function:

```javascript
SkiDayCount.propTypes = {
    total: PropTypes.number.isRequired,
    powder: PropTypes.number,
    backcountry: PropTypes.number,
    goal: PropTypes.number
}
```

### Custom validation
* We can use a function for validation

```javascript
SkiDayList.propTypes = {
    days: function(props) {
        if (!Array.isArray(props.days)) {
            return new Error("SkiDayList should be an array");
        } else if (!props.days.length) {
            return new Error("SkiDayList cannot be an empty array");
        } else {
            return null;
        }
    }
}
```

### Working with state
* If use createClass, we add one more function name `getInitialState()`

```javascript
export const App = createClass({
    getInitialState() {
        return {...}
    },
    ...
})
```
### State with ES6 classes
* We can init the state by assigning `this.state` to an object in the `constructor` function

## Chapter 05 - Using the React Router

### Incorporating the router
* Install react router: `npm install react-router`
* Code example

```javascript
import {Router, Route, hashHistory} from "react-router"

render(
	<Router history={hashHistory}>
        // "/" for the root path
        <Route path = "/" component={App}/>
        // other path => not found
        <Route path = "*" component={Whoops404}/>

    </Router>,
	document.getElementById('react-container')
)
```

* Note: From React Router 4.0.0, hashHistory isn't supported

### Navigating with the link component
* Example

```javascript
import {Link} from "react-router"

export const Menu = () => (
    <nav className = "menu">
        <Link to="/" activeClassName="selected">
            <HomeIcon/>
        </Link>
        <Link to="/add-day" activeClassName="selected">
            <AddDayIcon/>
        </Link>
        <Link to="/list-days" activeClassName="selected">
            <ListDaysIcon/>
        </Link>

    </nav>
)
```

### Using route parameters
* Similar to `express` module

### Nesting routes
* The Route component can be nested in another Route component

## Chapter 06 - -Forms and Refs

### Creating a form component
* Similar to form in HTML, but it has some difference
    * `for` is replaced by `htmlFor`
    * `class` is replaced by `className`
    * We use `defaultValue` or `defaultChecked` to set the default

### Using refs in class components
* We add `ref` attribute to element
* Example

```html
<input id="name"
       type="text"
       ref="ref_name"/>
```
* We can use ref with syntax: `this.ref.ref_name`

### Using refs in stateless components
* In stateless components, we can't keyword `this`
* Example

```javascript
export const Form = () => {

    let _name, _age
    const submit = (e) => {
            e.preventDefault();
            console.log("name", _name.value);
            console.log("age", _age.value);
    }

    return (
        <form className="add-info-form"
              onSubmit={submit}>
            <label htmlFor="resort">Name</label>
            <input id="resort"
                   type="text"
                   ref={input => _name = input}/>

            <label htmlFor="date">Date</label>
            <input id="date"
                   type="date"
                   ref={input => _age = input}/>
            <button>Add Day</button>
        </form>
    )
}
```

### Adding an autocomplete component

```javascript
class Autocomplete extends Component {

    get value() {
        return this.refs.inputResort.value
    }

    set value(inputValue) {
        this.refs.inputResort.value = inputValue
    }

    render() {
        return(
            <div>
                <input ref="inputResort" type="text" list="tahoe-resorts"/>
                <datalist id="tahoe-resorts">
                    {this.props.options.map(
                        (opt,i) =>
                            <option key={i}>
                                {opt}
                            </option>
                    )}
                </datalist>
            </div>
        )
    }
}
```

## Chapter 07 - The Component Lifecycle

### Understanding the mounting lifecycle
* These methods are called when an instance of a component is being created and inserted into the DOM:
    * `constructor()`
    * `componentWillMount()`
    * `render()`
    * `componentDidMount()`

### Understanding the updating lifecycle
* An update can be caused by changes to props or state. These methods are called when a component is being re-rendered:
    * `componentWillReceiveProps()`
    * `shouldComponentUpdate()`
    * `componentWillUpdate()`
    * `render()`
    * `componentDidUpdate()`
