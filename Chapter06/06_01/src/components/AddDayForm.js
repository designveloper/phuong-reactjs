import { PropTypes, Component } from 'react'

export class AddDayForm extends Component {
    render() {

        const {resort, date, powder, backcountry} = this.props

        return (
            <form className="add-day-form">
                <label hmtlFor="resort">Resort Name</label>
                <input id="resort" type="text" required defaultValue={resort}/>

                <label hmtlFor="date">Date</label>
                <input id="date" type="date" required defaultValue={date}/>

                <div>
                    <input id="powder" type="checkbox" required defaultChecked={powder}/>
                    <label hmtlFor="powder">Powder Day</label>
                </div>
                <div>
                    <input id="backcountry" type="checkbox" required defaultChecked={backcountry}/>
                    <label hmtlFor="backcountry">Backcountry Day</label>
                </div>

            </form>
        )
    }
}

AddDayForm.defaultProps = {
    resort: "Kirlwood",
    date: "2017-02-12",
    powder: true,
    backcountry: false
}

AddDayForm.propTypes = {
	resort: PropTypes.string.isRequired,
	date: PropTypes.string.isRequired,
	powder: PropTypes.bool.isRequired,
	backcountry: PropTypes.bool.isRequired
}
